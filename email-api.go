package main

import (
	"fmt"

	"gitlab.com/pbedat/go-reading-list/core"
)

type emailApiConfig struct {
	url               string
	restrictFrom      []string
	storage           *Storage
	followers         *core.FollowerService
	bookmarkTransport core.BookmarkTransport
}

func runEmailAPI(c emailApiConfig) error {
	mails := streamMails(c.url)
	bookmarks := core.ParseBookmarks(mails, c.restrictFrom)

	for b := range bookmarks {

		err := c.storage.append(b)

		if err != nil {
			return fmt.Errorf("error: unable to append entry\n> %s", err)
		}

		go func() {
			c.bookmarkTransport.Send(b, c.followers.Followers())
		}()
	}

	return nil
}
