package frontend

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/core"
	"gitlab.com/pbedat/go-reading-list/util"
)

func followPage(c *gin.Context) {
	followView(c, nil)
}

func unsubscribeAction(c *gin.Context) {
	s := c.MustGet("auth.session").(core.Session)
	err := util.ProvideFollowers(c).Follow(s.Email, false)

	followView(c, err)
}

func followView(c *gin.Context, err error) {
	session := c.MustGet("auth.session").(core.Session)
	f := util.ProvideFollowers(c)

	status := http.StatusOK

	if err != nil {
		status = http.StatusBadRequest
	}

	m := gin.H{
		"email":  session.Email,
		"follow": f.IsFollowing(session.Email),
		"saved":  c.Request.Method == "POST",
	}

	if err != nil {
		m["error"] = err
	}

	c.HTML(status, "follow.tmpl", m)
}

type followFormData struct {
	Follow bool `form:"follow"`
}

func submitFollowAction(c *gin.Context) {

	session := c.MustGet("auth.session").(core.Session)

	var d followFormData

	err := c.Bind(&d)

	fmt.Printf("%v\n", d)

	f := util.ProvideFollowers(c)

	if err != nil {
		followView(c, err)
		return
	}

	err = f.Follow(session.Email, d.Follow)

	followView(c, err)
}
