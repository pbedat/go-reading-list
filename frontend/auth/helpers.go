package auth

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/core"
	"gitlab.com/pbedat/go-reading-list/util"
)

type contextHelper struct {
	*gin.Context
}

func helper(c *gin.Context) contextHelper {
	return contextHelper{
		Context: c,
	}
}

func (h contextHelper) isAuthenticated() bool {
	auth := util.ProvideAuth(h.Context)

	sessionID, err := h.getSessionID()

	if err != nil {
		return false
	}

	if _, err := auth.Session(sessionID); err != nil {
		return false
	}

	return true
}

func (h contextHelper) redirectToErrorPage(err error) {
	h.Context.Redirect(http.StatusFound, fmt.Sprintf("/authenticate/error?err=%s", err))
}

func (h contextHelper) getSessionID() (string, error) {
	sessionID, err := h.Context.Cookie("session-id")

	if err != nil {
		return "", fmt.Errorf("can't retrieve 'session-id' cookie")
	}

	return sessionID, nil
}

func (h contextHelper) getSession() (core.Session, error) {
	sessionID, err := h.getSessionID()

	if err != nil {
		return core.Session{}, err
	}

	return util.ProvideAuth(h.Context).Session(sessionID)
}
