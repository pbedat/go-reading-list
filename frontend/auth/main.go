package auth

import "github.com/gin-gonic/gin"

const (
	pathSuccess = "/authenticate/success"
)

const sessionCookieName = "session-id"

func RegisterAuth(r *gin.Engine) {

	unauthenticated := r.Group("/authenticate", skipIfAuthenticated)
	{
		unauthenticated.GET("/", authenticationPage)
		unauthenticated.POST("/", authenticateAction)
		unauthenticated.GET("/confirm", waitForConfirmationPage)
		unauthenticated.GET("/confirm/:id", confirmAction)
		unauthenticated.GET("/error", authErrorPage)
	}

	r.GET(pathSuccess, authSuccessPage)

}
