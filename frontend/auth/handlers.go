package auth

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/core"
	"gitlab.com/pbedat/go-reading-list/util"
)

type authFormData struct {
	Email  string `form:"email" binding:"required,email"`
	Intent string `form:"intent"`
}

func authenticationPage(c *gin.Context) {
	intent := c.Query("intent")

	if intent == "" {
		intent = "/"
	}

	c.HTML(http.StatusOK, "authenticate.tmpl", gin.H{
		"intent": intent,
	})
}

func authenticateAction(c *gin.Context) {

	var payload authFormData

	err := c.Bind(&payload)

	if err != nil {
		c.HTML(http.StatusBadRequest, "authenticate.tmpl", gin.H{
			"err":   err,
			"email": payload.Email,
		})
		return
	}

	_, err = util.ProvideAuth(c).CreateHandshake(payload.Email, payload.Intent)

	if err != nil {
		c.HTML(http.StatusBadRequest, "authenticate.tmpl", gin.H{
			"err":   err,
			"email": payload.Email,
		})
		return
	}

	c.Redirect(
		http.StatusFound, fmt.Sprintf("/authenticate/confirm?email=%s", payload.Email),
	)
}

func waitForConfirmationPage(c *gin.Context) {

	var d authFormData

	err := c.BindQuery(&d)

	if err != nil {
		c.HTML(http.StatusBadRequest, "confirm.tmpl", gin.H{
			"err": err,
		})
		return
	}

	h := util.ProvideAuth(c).Handshake(d.Email)

	switch h.State() {
	case core.Confirmed:
		fallthrough
	case core.Waiting:
		c.HTML(200, "confirm.tmpl", gin.H{
			"email": d.Email,
			"state": h.State().String(),
		})
		return
	}

	helper(c).redirectToErrorPage(fmt.Errorf("invalid handshake state '%s'", h.State()))
	return
}

func confirmAction(c *gin.Context) {
	s, err := util.ProvideAuth(c).Confirm(c.Param("id"))

	if err != nil {
		helper(c).redirectToErrorPage(fmt.Errorf("/authenticate/error?err=%s", err))
		return
	}

	c.SetCookie(sessionCookieName, s.Id, 24*3600, "/", c.Request.URL.Hostname(), false, true)
	c.Redirect(http.StatusFound, pathSuccess)
}

func authErrorPage(c *gin.Context) {
	err := c.Query("err")

	c.HTML(http.StatusBadRequest, "authenticate-error.tmpl", gin.H{"error": err})
}

func authSuccessPage(c *gin.Context) {

	s, err := helper(c).getSession()

	fmt.Printf("%v\n", err)

	if err != nil {
		c.Redirect(http.StatusFound, "/authenticate")
		return
	}

	c.HTML(http.StatusOK, "authenticate-success.tmpl", gin.H{
		"intent": s.Intent,
	})
}
