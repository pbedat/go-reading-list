package auth

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/util"
)

func skipIfAuthenticated(c *gin.Context) {

	auth := util.ProvideAuth(c)

	sessionID, err := helper(c).getSessionID()

	if err != nil {
		c.Next()
		return
	}

	if _, err := auth.Session(sessionID); err != nil {
		c.Next()
		return
	}

	c.Redirect(http.StatusFound, pathSuccess)
	c.Abort()
}

func Authenticated(c *gin.Context) {
	s, err := helper(c).getSession()

	c.Set("auth.session", s)

	if err != nil {
		// delete the cookie (gins set cookie doesn't seem capable of this)
		http.SetCookie(c.Writer, &http.Cookie{
			Name:     sessionCookieName,
			Value:    "",
			Path:     "/",
			HttpOnly: true,
			Expires:  time.Unix(0, 0),
		})
		c.Redirect(http.StatusFound, fmt.Sprintf("/authenticate?intent=%s", c.FullPath()))
		c.Abort()
		return
	}

	c.Next()
}
