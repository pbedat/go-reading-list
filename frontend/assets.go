package frontend

import (
	"time"

	"github.com/jessevdk/go-assets"
)

var _Assetsaa259202725c141a5852e00aecdb8d02689622cf = "<html>\n\n<head>\n  <title>Follow</title>\n</head>\n\n<body>\n  {{if .error }}\n  <p>Error: {{.error}}</p>\n  {{end}}\n  {{if .saved}}\n  <p>Preferences successfully updated!</p>\n  {{end}}\n  <form action=\"{{baseUrl}}/follow\" method=\"POST\">\n    <p>\n      <label for=\"follow\">Receive notifications?</label>\n      <label><input type=\"radio\" name=\"follow\" value=\"true\" {{if .follow}}checked{{end}} />\n        Yes</label>\n      <label><input type=\"radio\" name=\"follow\" value=\"false\" {{if not .follow}}checked{{end}} />\n        No</label>\n    </p>\n    <p>\n      <button>Save preferences</button>\n    </p>\n  </form>\n</body>\n\n</html>"
var _Assetsd6b51b88ba8791a23692e8941b164ea8c39b9c9a = "<html>\n\n<body>\n  <meta http-equiv=\"refresh\" content=\"3;url={{baseUrl}}{{.intent}}\" />\n  <h1>Success!</h1>\n  Redirecting you to <a href=\"{{baseUrl}}{{.intent}}\">{{.intent}}</a> in a moment...\n</body>\n\n</html>"
var _Assets1ba01caace91409304881a70c33cb5b227aefb0a = "<html>\n  <head>\n    <title>Error</title>\n  </head>\n  <body>\n    <h1>Error</h1>\n    {{.error}}\n  </body>\n</html>\n"
var _Assets42ca724d51f93afd5a2075da09846e90fb9a753e = "<html>\n  <head>\n    <title>Waiting for confirmation</title>\n\n    <meta http-equiv=\"refresh\" content=\"5\" />\n  </head>\n  <body>\n    <h1>\n      Check your inbox!\n    </h1>\n    <alert>{{.err}}</alert>\n    <p>We sent you a confirmation email to {{.email}}</p>\n    <p>{{.state}}...</p>\n  </body>\n</html>\n"
var _Assets76b49d7c50d83bb2454d7349b66228b5fd047416 = "<html>\n\n<head>\n  <title>Who are you?</title>\n</head>\n\n<body>\n  <alert>{{ .err }}</alert>\n  <h1>Who are you?</h1>\n  <form method=\"POST\" action=\"{{baseUrl}}/authenticate\">\n    <input type=\"hidden\" name=\"intent\" value=\"{{.intent}}\" />\n    <p>\n      <label for=\"email\">\n        Email:\n      </label>\n      <input name=\"email\" type=\"text\" value=\"{{.email}}\" />\n    </p>\n    <p>\n      <button>\n        Authenticate\n      </button>\n    </p>\n  </form>\n</body>\n\n</html>"

// Assets returns go-assets FileSystem
var Assets = assets.NewFileSystem(map[string][]string{}, map[string]*assets.File{
	"confirm.tmpl": &assets.File{
		Path:     "confirm.tmpl",
		FileMode: 0x1a4,
		Mtime:    time.Unix(1587319549, 1587319549332711178),
		Data:     []byte(_Assets42ca724d51f93afd5a2075da09846e90fb9a753e),
	}, "authenticate.tmpl": &assets.File{
		Path:     "authenticate.tmpl",
		FileMode: 0x1a4,
		Mtime:    time.Unix(1587319549, 1587319549332711178),
		Data:     []byte(_Assets76b49d7c50d83bb2454d7349b66228b5fd047416),
	}, "follow.tmpl": &assets.File{
		Path:     "follow.tmpl",
		FileMode: 0x1a4,
		Mtime:    time.Unix(1587319549, 1587319549332711178),
		Data:     []byte(_Assetsaa259202725c141a5852e00aecdb8d02689622cf),
	}, "authenticate-success.tmpl": &assets.File{
		Path:     "authenticate-success.tmpl",
		FileMode: 0x1a4,
		Mtime:    time.Unix(1587319549, 1587319549332711178),
		Data:     []byte(_Assetsd6b51b88ba8791a23692e8941b164ea8c39b9c9a),
	}, "authenticate-error.tmpl": &assets.File{
		Path:     "authenticate-error.tmpl",
		FileMode: 0x1a4,
		Mtime:    time.Unix(1587319549, 1587319549332711178),
		Data:     []byte(_Assets1ba01caace91409304881a70c33cb5b227aefb0a),
	}}, "")
