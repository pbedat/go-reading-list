package frontend

import (
	"github.com/gin-gonic/gin"
)

type action struct {
	path    string
	handler gin.HandlerFunc
}

type context gin.Context
