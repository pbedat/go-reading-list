package frontend

import (
	"fmt"
	"html/template"
	"io"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/util"
)

func getEntries(baseUrl string) gin.HandlerFunc {

	tpl, err := template.New("bookmark").Parse(`  <li class="reading-list__item">
	<a class="reading-list__link" href="{{.Link}}" target="_blank">{{.Title}}</a> 
	({{ .FormattedDate }})
	</li>`)

	if err != nil {
		panic(err)
	}

	return func(c *gin.Context) {

		s := util.ProvideStorage(c)

		bookmarks, err := s.StreamEntries()

		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		c.Header("Content-Type", "text/html")
		c.Header("Cache-Control", "max-age=3600") // 1 hour

		modTime, err := s.LastModified()

		if err != nil {
			c.AbortWithError(500, err)
			return
		}

		etag := fmt.Sprintf(`W/"%s"`, modTime.Format("20060102150405"))

		ifNoneMatch := c.Request.Header.Get("If-None-Match")

		if ifNoneMatch == etag {
			c.Status(304)
			return
		}

		c.Header("ETag", etag)

		fmt.Fprintf(c.Writer, `<p><a href="%s/follow">Follow</a></p>`, baseUrl)

		fmt.Fprintln(c.Writer, `<ul class="reading-list">`)

		c.Stream(func(w io.Writer) bool {
			bookmark, ok := <-bookmarks

			if !ok {
				return false
			}
			err := tpl.Execute(w, bookmark)

			if err != nil {
				c.AbortWithError(500, err)
				return false
			}

			return true
		})

		fmt.Fprintln(c.Writer, "</ul>")
	}

}
