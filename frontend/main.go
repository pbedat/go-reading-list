package frontend

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/core"
	"gitlab.com/pbedat/go-reading-list/frontend/auth"
	"gitlab.com/pbedat/go-reading-list/util"
)

type FrontendConfig struct {
	BaseURL   string
	Port      int
	Debug     bool
	Storage   core.Storage
	Auth      *core.Authenticator
	Followers *core.FollowerService
}

// RunFrontend runs the http frontend
func RunFrontend(config FrontendConfig) error {

	if !config.Debug {
		gin.SetMode(gin.ReleaseMode)
	}

	port := config.Port

	srv := gin.Default()

	fns := template.FuncMap{
		"baseUrl": func() string {
			return config.BaseURL
		},
	}

	t, err := loadTemplate(fns)

	if err != nil {
		return err
	}

	srv.SetHTMLTemplate(t)

	srv.Use(func(c *gin.Context) {
		util.InjectStorage(c, config.Storage)
		util.InjectAuth(c, config.Auth)
		util.InjectFollowers(c, config.Followers)
		c.Next()
	})

	srv.GET("/", getEntries(config.BaseURL))

	srv.GET("/version", func(c *gin.Context) {
		fmt.Fprint(c.Writer, "v0.3.0")
	})

	srv.GET("/follow", auth.Authenticated, followPage)
	srv.GET("/unsubscribe", auth.Authenticated, unsubscribeAction)
	srv.POST("/follow", auth.Authenticated, submitFollowAction)

	auth.RegisterAuth(srv)

	return srv.Run(fmt.Sprintf(":%d", port))
}

func loadTemplate(funcMap template.FuncMap) (*template.Template, error) {
	t := template.New("").Funcs(funcMap)
	for name, file := range Assets.Files {
		if file.IsDir() || !strings.HasSuffix(name, ".tmpl") {
			continue
		}
		h, err := ioutil.ReadAll(file)
		if err != nil {
			return nil, err
		}
		t, err = t.New(name).Funcs(funcMap).Parse(string(h))
		if err != nil {
			return nil, err
		}
	}
	return t, nil
}
