package core

import (
	"log"
	"strings"
	"time"
)

func ParseBookmarks(mails chan Email, restrictFrom []string) chan Bookmark {
	bookmarks := make(chan Bookmark)

	go func() {
		defer close(bookmarks)

		for m := range mails {

			from := m.From.Value[0].Address

			if len(restrictFrom) == 0 {
				goto processMail
			}

			for _, address := range restrictFrom {
				if address == from {
					goto processMail
				}
			}

			log.Printf("rejecting email from '%s'", from)

			continue

		processMail:
			// we expect the link in the first line of the email text
			link := strings.Split(m.Text, "\n")

			if len(link) < 1 {
				log.Println("error: can't parse link - empty email text")
				continue
			}

			if len(m.Subject) == 0 {
				log.Println("error: can't parse title - empty email subject")
				continue
			}

			b := Bookmark{
				Title: m.Subject,
				Link:  link[0],
				Date:  time.Now(),
			}

			bookmarks <- b
		}
	}()

	return bookmarks
}

// Bookmark ...
type Bookmark struct {
	Title string    `json:"title"`
	Link  string    `json:"link"`
	Date  time.Time `json:"date"`
}

// FormattedDate dd.mm.yyyy
func (b Bookmark) FormattedDate() string {
	return b.Date.Format("2006-01-02")
}
