package core

import (
	"fmt"
	"time"

	"github.com/google/uuid"
)

type Authenticator struct {
	handshakes handshakes
	sessions   sessions

	transport ConfirmationTransport

	NewSessions chan Session
}

type Session struct {
	Id     string
	Email  string
	Intent string
}

type sessions []Session

func NewAuthenticator(transport ConfirmationTransport) *Authenticator {
	return &Authenticator{
		make([]handshake, 0),
		make(sessions, 0),
		transport,
		make(chan Session),
	}
}

type HandshakeState int

func (s HandshakeState) String() string {
	switch s {
	case Waiting:
		return "Waiting"
	case Confirmed:
		return "Confirmed"
	case Expired:
		return "Expired"
	}

	panic(fmt.Errorf("string of handshake state '%d' not implemented", s))
}

const (
	Waiting   HandshakeState = 0
	Confirmed HandshakeState = 1
	Expired   HandshakeState = 2
)

type handshakes []handshake

type handshake struct {
	id        string
	email     string
	state     HandshakeState
	createdAt time.Time
	intent    string
}

func (s handshake) Id() string {
	return s.id
}

func (s handshake) State() HandshakeState {
	return s.state
}

const maxHandshakes = 5

func (a *Authenticator) CreateHandshake(email string, intent string) (handshake, error) {

	a.handshakes = a.handshakes.expireExisting(email).expireTimedOut().clearOld()

	nHandshakes := 0
	for _, s := range a.handshakes {
		if s.email == email {
			nHandshakes++
		}
	}

	if nHandshakes == maxHandshakes {
		return handshake{}, fmt.Errorf("you have reached the limit of authentication sessions, please try again later")
	}

	h := handshake{
		uuid.New().String(),
		email,
		Waiting,
		time.Now(),
		intent,
	}

	a.transport.Send(
		email,
		h.id,
	)

	a.handshakes = append(a.handshakes, h)

	return h, nil
}

const sessionLifeTime = 10 * time.Minute

func (handshakes handshakes) expireExisting(email string) handshakes {
	for _, h := range handshakes {
		if h.email == email {
			fmt.Printf("existing session '%s' expired\n", h.id)
			h.state = Expired
		}
	}

	return handshakes
}

func (handshakes handshakes) expireTimedOut() handshakes {
	for _, h := range handshakes {
		if time.Now().Sub(h.createdAt) > 10*time.Minute {
			fmt.Printf("session '%s' timed out\n", h.id)
			h.state = Expired
		}
	}

	return handshakes
}

func (handshakes handshakes) clearOld() handshakes {
	next := make([]handshake, 0)
	for _, h := range handshakes {
		if time.Now().Sub(h.createdAt) < 30*time.Minute {
			fmt.Printf("session '%s' cleared\n", h.id)
			next = append(next, h)
		}
	}

	return next
}

func (a *Authenticator) Handshake(email string) handshake {

	for _, h := range a.handshakes {
		if h.email == email && h.state == Waiting {
			return h
		}
	}

	return handshake{
		id:    "",
		email: email,
		state: Expired,
	}
}

func (a *Authenticator) Confirm(id string) (Session, error) {

	var h handshake

	for _, h = range a.handshakes {
		if h.state == Waiting && h.id == id {
			break
		}
	}

	if h.state != Waiting || h.id != id {
		return Session{}, fmt.Errorf("no valid auth session available")
	}

	next := make(handshakes, len(a.handshakes))

	for i, s := range a.handshakes {
		if s.id != id {
			next[i] = s
		} else {
			s.state = Confirmed
			next[i] = s
		}
	}

	a.handshakes = next

	s := Session{
		uuid.New().String(),
		h.email,
		h.intent,
	}
	a.sessions = append(a.sessions, s)

	a.NewSessions <- s

	return s, nil
}

func (a *Authenticator) Session(id string) (Session, error) {
	for _, s := range a.sessions {

		if s.Id == id {
			return s, nil
		}
	}

	return Session{}, fmt.Errorf("session not found")
}
