package core

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"sort"
	"strings"
	"sync"
)

// FollowerService manages a text file under `path` with one follower email per line
type FollowerService struct {
	path      string
	followers []string

	mux sync.Mutex
}

// Followers returns all followers managed by the service
func (s *FollowerService) Followers() []string {
	return s.followers
}

// NewFollowerService creates a new service and initializes it by loading all followers
func NewFollowerService(p string) (*FollowerService, error) {

	_, err := os.Stat(p)

	if err != nil {
		if os.IsNotExist(err) {
			os.MkdirAll(path.Dir(p), os.ModePerm)
			ioutil.WriteFile(p, []byte{}, os.ModePerm)
		} else {
			return nil, err
		}
	}

	followers, err := loadFollowers(p)

	if err != nil {
		return nil, err
	}

	return &FollowerService{
		p,
		followers,
		sync.Mutex{},
	}, nil
}

func loadFollowers(path string) ([]string, error) {
	buff, err := ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	str := string(buff)

	if str == "" {
		return []string{}, nil
	}

	followers := strings.Split(str, fmt.Sprintln())

	return followers, nil
}

// Follow removes or inserts an email from or into the follower list
func (s *FollowerService) Follow(email string, follow bool) error {

	s.mux.Lock()
	defer s.mux.Unlock()

	i := sort.SearchStrings(s.followers, email)

	if follow {
		alreadyExists := i < len(s.followers) && s.followers[i] == email
		if alreadyExists {
			return nil
		}

		s.followers = append(s.followers, "")
		copy(s.followers[i+1:], s.followers[i:])
		s.followers[i] = email
	} else {
		if len(s.followers) > 0 {
			s.followers = append(s.followers[:i], s.followers[i+1:]...)
		}
	}

	return ioutil.WriteFile(s.path, []byte(strings.Join(s.followers, fmt.Sprintln())), os.ModePerm)
}

// IsFollowing returns true when `email` is in the follower list
func (s *FollowerService) IsFollowing(email string) bool {
	i := sort.SearchStrings(s.followers, email)

	return i < len(s.followers) && s.followers[i] == email
}
