package core

import "time"

type Storage interface {
	LastModified() (time.Time, error)
	StreamEntries() (chan Bookmark, error)
}

type BookmarkTransport interface {
	Send(b Bookmark, emails []string)
}
