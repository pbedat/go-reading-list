package core

type Email struct {
	From    address `json:"from"`
	Subject string  `json:"subject"`
	Text    string  `json:"text"`
}

type address struct {
	Text  string         `json:"text"`
	HTML  string         `json:"html"`
	Value []addressValue `json:"value"`
}

type addressValue struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

type ConfirmationTransport interface {
	Send(email string, token string) error
}
