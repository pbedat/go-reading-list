package util

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pbedat/go-reading-list/core"
)

func InjectStorage(c *gin.Context, s core.Storage) {
	c.Set("storage", s)
}

func ProvideStorage(c *gin.Context) core.Storage {
	return c.MustGet("storage").(core.Storage)
}

func InjectAuth(c *gin.Context, a *core.Authenticator) {
	c.Set("auth", a)
}

func ProvideAuth(c *gin.Context) *core.Authenticator {
	return c.MustGet("auth").(*core.Authenticator)
}

func InjectFollowers(c *gin.Context, f *core.FollowerService) {
	c.Set("followers", f)
}

func ProvideFollowers(c *gin.Context) *core.FollowerService {
	return c.MustGet("followers").(*core.FollowerService)
}
