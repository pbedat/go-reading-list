package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestStorage(t *testing.T) {
	s, err := makeStorage("/tmp/bar")

	defer os.Remove("/tmp/bar")

	if err != nil {
		t.Errorf("unexpected error %s", err)
	}

	fmt.Println("LUL")

	s.append(foo{"test1"})
	s.append(foo{"test2"})

	f, _ := ioutil.ReadFile("/tmp/bar")

	str := string(f)

	want := `{"Bar":"test1"}
{"Bar":"test2"}
`

	if str != want {
		t.Errorf("unexpected file contents\n%s", cmp.Diff(str, want))
	}
}

func TestEntries(t *testing.T) {
	s, _ := makeStorage("/tmp/foo")

	defer os.Remove("/tmp/foo")

	s.append("foo")
	s.append("bar")

	entries, err := s.StreamEntries()

	if err != nil {
		fmt.Println(err)
	}

	for e := range entries {
		fmt.Printf("%v\n", e)
	}
}

type foo struct {
	Bar string
}
