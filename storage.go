package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"sync"
	"time"

	"github.com/serverhorror/rog-go/reverse"
	"gitlab.com/pbedat/go-reading-list/core"
)

// Storage wraps a simple file based storage
type Storage struct {
	Path string
	mux  sync.Mutex
}

// makeStorage creates a new file storage at `path`
func makeStorage(p string) (*Storage, error) {

	err := os.MkdirAll(path.Dir(p), os.ModePerm)

	if err != nil {
		return nil, err
	}

	f, err := os.OpenFile(p, os.O_CREATE, 0644)

	defer f.Close()

	if err != nil {
		return nil, err
	}

	return &Storage{
		p,
		sync.Mutex{},
	}, nil
}

// append appends arbitrary data to the file, serialized as JSON
func (s *Storage) append(item interface{}) error {

	s.mux.Lock()
	defer s.mux.Unlock()

	file, err := os.OpenFile(s.Path, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0644)

	if err != nil {
		return fmt.Errorf("can't open file %s", err)
	}

	defer file.Close()

	json, err := json.Marshal(item)

	if err != nil {
		return fmt.Errorf("unable to marshal item %v", item)
	}

	_, err = file.Write(append(json, ([]byte("\n"))...))

	if err != nil {
		return fmt.Errorf("can't write to file %s", err)
	}

	return nil
}

// LastModified file last modified date
func (s *Storage) LastModified() (time.Time, error) {
	stat, err := os.Stat(s.Path)

	if err != nil {
		return time.Now(), err
	}

	return stat.ModTime(), nil
}

// StreamEntries reads all entries from the storage file
func (s *Storage) StreamEntries() (chan core.Bookmark, error) {
	s.mux.Lock()

	defer s.mux.Unlock()

	file, err := os.Open(s.Path)

	if err != nil {
		return nil, fmt.Errorf("can't open storage file '%s'\n> %s", s.Path, err)
	}

	scanner := reverse.NewScanner(file)
	// scanner := bufio.NewScanner(file)

	c := make(chan core.Bookmark)

	go func() {
		defer close(c)
		defer file.Close()

		for scanner.Scan() {
			jstr := scanner.Text()
			var item core.Bookmark
			err := json.Unmarshal([]byte(jstr), &item)

			if err != nil {
				log.Printf("can't parse json string from file '%s'\n> %s\n", s.Path, err)
			} else {
				c <- item
			}
		}

		if err := scanner.Err(); err != nil {
			log.Printf("error: %s", err)
		}
	}()

	return c, nil
}
