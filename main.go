// Package main contains the reading-list application
package main

import (
	"fmt"
	"log"
	"os"
	"path"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
	"gitlab.com/pbedat/go-reading-list/core"
	"gitlab.com/pbedat/go-reading-list/frontend"
	"gitlab.com/pbedat/go-reading-list/transport"
)

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

var source, dataDir string
var restrictFrom []string
var port int
var debug bool
var smtpURI string
var smtpFrom string
var baseUrl string

func init() {
	rootCmd.Flags().StringVarP(&source, "source", "s", "", "the source event stream URL of the reading list")
	rootCmd.MarkFlagRequired("source")

	rootCmd.Flags().StringVarP(&dataDir, "data-dir", "d", "", "path where the app data is stored")
	rootCmd.MarkFlagRequired("path")

	rootCmd.Flags().IntVar(&port, "port", 8080, "port of the frontend")
	rootCmd.Flags().BoolVar(&debug, "debug", false, "debug mode")
	rootCmd.Flags().StringSliceVar(&restrictFrom, "restrict-from", []string{}, "Restricts the usage of the reading list to the specified email addresses")

	rootCmd.Flags().StringVar(&smtpURI, "smtp-uri", "", "e.g. smtps://user:pw@mail-host.com")
	rootCmd.MarkFlagRequired("smtp-uri")

	rootCmd.Flags().StringVar(&smtpFrom, "smtp-from", "", "e.g. foo@bar.net")
	rootCmd.MarkFlagRequired("smtp-from")

	rootCmd.Flags().StringVar(&baseUrl, "base-url", "", "http://pbedat.de/reading-list")
}

var rootCmd = &cobra.Command{
	Use:   "readinglist",
	Short: "The reading list client",
	Run: func(cmd *cobra.Command, args []string) {

		if debug {
			cmd.DebugFlags()
		}

		storage, err := makeStorage(path.Join(dataDir, "bookmarks"))

		if err != nil {
			log.Fatalf("can't initialize storage on file '%s'\n> %s\n", dataDir, err)
		}

		confirmTransport, err := transport.NewSmtpConfirmTransport(smtpURI, smtpFrom, baseUrl)

		if err != nil {
			panic(err)
		}

		auth := core.NewAuthenticator(confirmTransport)

		if err != nil {
			panic(err)
		}

		followers, err := core.NewFollowerService(path.Join(dataDir, "followers"))

		if err != nil {
			panic(err)
		}

		bookmarkTransport, err := transport.NewSmtpBookmarkTransport(transport.SmtpBookmarkTransportConfig{
			smtpURI,
			smtpFrom,
			log.New(gin.DefaultErrorWriter, "transport", log.Ldate|log.Ltime|log.Lshortfile),
		})

		go func() {
			for s := range auth.NewSessions {
				if s.Intent == "/follow" {
					followers.Follow(s.Email, true)
				}
			}
		}()

		c := make(chan error)

		go func() {
			err := runEmailAPI(emailApiConfig{
				source, restrictFrom, storage,
				followers,
				bookmarkTransport,
			})
			c <- err
		}()

		go func() {
			err := frontend.RunFrontend(frontend.FrontendConfig{
				baseUrl,
				port,
				debug,
				storage,
				auth,
				followers,
			})
			c <- err
		}()

		err = <-c

		if err != nil {
			log.Fatalf("error: %s", err)
		}
	},
}
