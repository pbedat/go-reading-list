#!/bin/bash

if command -v inotifywait; then 
  echo "let's go!"
else
  echo "error: this script requires 'inotifywait' from 'inotify-tools'"
  exit 1
fi

# handle ctrl+c (SIGINT)
trap handle_int INT

handle_int() {
    echo "exiting..."
    kill "$PID"
    exit
}


while true; do
    # reads $SMTP_URI
    source ".env"
    ~/go/bin/go-assets-builder -s "/templates/" -p frontend -o frontend/assets.go templates
    go build

    if [ $? -eq 0 ]
    # when the build was successful
    then
        ./go-reading-list \
            --source https://mail-fu.pbedat.de/api/pbedat.de/go-reading-list/events \
            --data-dir .data \
            --port 8080 \
            --restrict-from "pbedat@gmail.com" \
            --smtp-uri $SMTP_URI \
            --smtp-from "pbedat@gmail.com" \
            --debug &

        PID=$!

        echo "$PID"

        #restart upon file change
        inotifywait --exclude '.(git|data)' -r -e modify .
        kill "$PID"

    #when the build fails
    else    
        inotifywait --exclude '.(git|data)' -r -e modify .
    fi
done
