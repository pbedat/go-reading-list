package transport

import (
	"fmt"

	"github.com/go-gomail/gomail"
	"gitlab.com/pbedat/go-reading-list/core"
)

type SmtpConfirmationTransport struct {
	SmtpTransport
	BaseUrl string
}

func (t *SmtpConfirmationTransport) Send(email string, token string) error {

	confirmLink := fmt.Sprintf("%s/authenticate/confirm/%s", t.BaseUrl, token)

	m := gomail.NewMessage()

	m.SetHeader("From", t.from)
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Confirming Your Identity")
	m.SetBody(
		"text/html",
		fmt.Sprintf(
			`<p><a href="%s">Click to confirm</a></p>`,
			confirmLink,
		),
	)

	m.AddAlternative(
		"text/plain",
		fmt.Sprintf(
			"Visit %s to confirm your identity",
			confirmLink,
		),
	)

	d := gomail.NewDialer(t.host, t.port, t.user, t.password)

	return d.DialAndSend(m)
}

func NewSmtpConfirmTransport(smtpUri string, from string, baseUrl string) (core.ConfirmationTransport, error) {
	smtpTransport, err := makeSmtpTransport(smtpUri, from)

	if err != nil {
		return nil, err
	}

	return &SmtpConfirmationTransport{
		smtpTransport,
		baseUrl,
	}, nil
}
