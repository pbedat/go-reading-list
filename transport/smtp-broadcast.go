package transport

import (
	"fmt"
	"log"
	"time"

	"github.com/go-gomail/gomail"
	"gitlab.com/pbedat/go-reading-list/core"
)

type SmtpBookmarkTransport struct {
	SmtpTransport
	logger *log.Logger
}

func (s SmtpBookmarkTransport) Send(b core.Bookmark, emails []string) {

	for _, email := range emails {
		time.Sleep(1 * time.Second)

		m := gomail.NewMessage()

		m.SetHeader("From", s.from)
		m.SetHeader("To", email)
		m.SetHeader("Subject", "New Reading List Entry on pbedat.de")
		m.SetBody(
			"text/html",
			fmt.Sprintf(
				`<p>There is a new reading list entry over at <a href="https://pbedat.de/reading-list">pbedat.de</a>:</p>
				<p><strong><a href="%s">%s</a></strong></p>
				<p><small>Click <a href="https://pbedat.de/app/reading-list/unsubscribe">here</a> to unsubscribe.</small></p>`,
				b.Link,
				b.Title,
			),
		)

		d := gomail.NewDialer(s.host, s.port, s.user, s.password)

		err := d.DialAndSend(m)

		if err != nil {
			s.logger.Printf("unable to transmit bookmark by email: %s\n", err)
		}
	}
}

type SmtpBookmarkTransportConfig struct {
	SmtpUri string
	From    string
	Logger  *log.Logger
}

func NewSmtpBookmarkTransport(config SmtpBookmarkTransportConfig) (core.BookmarkTransport, error) {

	smtpTransport, err := makeSmtpTransport(config.SmtpUri, config.From)

	if err != nil {
		return nil, err
	}

	return &SmtpBookmarkTransport{
		SmtpTransport: smtpTransport,
		logger:        config.Logger,
	}, nil
}
