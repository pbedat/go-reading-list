package transport

import (
	"net/url"
	"strconv"
)

type SmtpTransport struct {
	host     string
	port     int
	user     string
	password string
	from     string
}

func makeSmtpTransport(smtpUri string, from string) (SmtpTransport, error) {
	url, err := url.Parse(smtpUri)

	if err != nil {
		return SmtpTransport{}, err
	}

	port := 25

	if url.Port() != "" {
		p, err := strconv.ParseUint(url.Port(), 10, 64)
		port = int(p)
		panic(err)
	}

	pw, _ := url.User.Password()

	return SmtpTransport{
		url.Host,
		port,
		url.User.Username(),
		pw,
		from,
	}, nil
}
