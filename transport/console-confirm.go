package transport

import (
	"fmt"

	"gitlab.com/pbedat/go-reading-list/core"
)

type ConsoleConfirmationTransport struct {
}

func (t *ConsoleConfirmationTransport) Send(email string, body string) error {
	fmt.Printf("CONFIRM\nto: %s\nbody: %s\n", email, body)

	return nil
}

func NewConsoleConfirmTransport() core.ConfirmationTransport {

	return &ConsoleConfirmationTransport{}
}
