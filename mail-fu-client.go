package main

import (
	"encoding/json"
	"log"
	"os"

	"github.com/donovanhide/eventsource"
	"gitlab.com/pbedat/go-reading-list/core"
)

func streamMails(url string) chan core.Email {
	stream, err := eventsource.Subscribe(url, "")

	if err != nil {
		log.Fatalf("error: unable to subscribe to url '%s'\n> %s\n", url, err)
	}

	stream.Logger = log.New(os.Stderr, "[eventsource]", log.LstdFlags)

	c := make(chan core.Email)

	go func() {
		defer close(c)

		for ev := range stream.Events {
			// ping events are meant to keep the connection alive and are ignored
			if ev.Event() == "ping" {
				continue
			}

			var m core.Email
			json.Unmarshal([]byte(ev.Data()), &m)

			c <- m
		}
	}()

	return c
}
